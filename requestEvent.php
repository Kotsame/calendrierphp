<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Les request php</title>
</head>
<body>

<?php
		session_start();	

            if ( isset( $_REQUEST['nom'] ) && $_REQUEST['nom'] ){
                echo '<p>Nom de l\'événement: ' . $_REQUEST['nom']. '</p>';
                $eventNom = ($_REQUEST['nom']);
            }
            
            if ( isset($_REQUEST['date']) && $_REQUEST['date'] ){
                echo '<p>Enregistré pour le: ' . $_REQUEST['date'] . '</p>';
                $eventDate = ($_REQUEST['date']);
            }
            
            if ( isset($_FILES['image']) && $_FILES['image']['size'] )
            {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $image = $_FILES['image'];
                if (finfo_file($finfo, $_FILES['image']['tmp_name']) == 'image/jpeg')
                {
                    $imageName = uniqid().'_'.$image['name'];
                    move_uploaded_file($image['tmp_name'], 'imagestéléchargés/'.$imageName);
                }
            }

            if ( isset( $_REQUEST['saveMethod'] ) && $_REQUEST['saveMethod'] )
            {
                echo '<p>Enregistré en: ' . $_REQUEST['saveMethod'] . '</p>';
                $saveMethod = ($_REQUEST['saveMethod']);

                if ( $saveMethod === 'saveCookie')
                {
                    if(isset($_COOKIE['Events'])){
                        $eventList = unserialize($_COOKIE['Events']);
                    }
    
                    //Ajout l'evenement à la liste
                    $eventList[] = array(
                        'Titre'=>$eventNom,
                        'Image'=>$imageName,
                        'TimeStamp'=>strtotime($eventDate)
                    ); 
    
                    //Serialize tab pout transformer le tableau en chaine de caractère
                    $serializeEvents = serialize($eventList);
    
                    
                    setcookie('Events',$serializeEvents,time()+60*60*24*30);

                    header('Location: calendrierv3.php');
                    
                }
                if ( $saveMethod === 'saveSession')
                {
                    
                    if(isset($_SESSION['Events'])){
                        $eventList = $_SESSION['Events'];
                    }
    
                    //Ajout l'evenement à la liste
                    $eventList[] = array(
                        'Title'=>$eventNom,
                        'Image'=>$imageName,
                        'TimeStamp'=>strtotime($eventDate)
                    );
    
                    //Sauvgarder en session
                    $_SESSION['Events'] = $eventList;

                    header('Location: calendrierv3.php');
                }					
            }
            


            
        ?>  
?>

</body>
</html>